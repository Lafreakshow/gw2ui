<fr-settings>
    <div class="container">
        <label for="sale_limes">Angebot
            <input id="sale_limes" name="sale_limes" type="number" min="0" value="1000">
        </label>
        <label for="offer_limes">Nachfrage
            <input id="offer_limes" name="offer_limes" type="number" min="0" value="1000">
        </label>
        <label for="diff_limes">Profit
            <input id="diff_limes" name="diff_limes" type="number" min="0" value="2">
        </label>
    </div>
    <span onclick={rl} id="reload_data_button">Daten neu Laden</span>
    <script>
    this.setOfferLimesInput = (val) => {
        this.offer_limes.value = val;
    };
    this.setSaleLimesInput = (val) => {
        this.sale_limes.value = val;
    };
    this.setDiffLimesInput = (val) => {
        this.diff_limes.value = val;
    };

    this.getOfferLimes = () => {
        return this.offer_limes.value;
    };
    this.getSaleLimes = () => {
        return this.sale_limes.value;
    };
    this.getDiffLimes = () => {
        return this.diff_limes.value;
    };
    this.rl = () => {
        this.trigger("reload_data");
    };
    </script>
</fr-settings>
