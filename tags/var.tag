<fr-item-list>
    <table class="tablesorter" name="itemtable" id="item_table">
        <thead>
            <tr class="head">
                <th id="li_id" show={ show.id } class="item">ID</th>
                <th id="li_name" show={ show.name } class="item">Name</th>
                <th id="li_rarity" show={ show.rarity } class="item">rarity</th>
                <th id="li_restriction" show={ show.restriction_level } class="item">restriction level</th>
                <th id="li_type" show={ show.type } class="item">type:sub-type</th>
                <th id="li_last_price_change" show={ show.last_price_change } class="item">Letzte Preisänderung</th>

                <th id="li_offer_price" show={ show.offer_price } class="item">Angebotspreis</th>
                <th id="li_offer_price_change_hour" show={ show.offer_price_change_last_hour } class="item">Bew. (%)</th>
                <th id="li_offer_price_change_hour_raw" show={ show.offer_price_change_last_hour_raw } class="item">Bew.</th>
                
                <th id="li_sale_price" show={ show.sale_price } class="item">Nachfragepreis</th>
                <th id="li_sale_price_change_hour" show={ show.sale_price_change_last_hour } class="item">Bew. (%)</th>
                <th id="li_sale_price_change_hour_raw" show={ show.sale_price_change_last_hour_raw } class="item">Bew.</th>
                
                <th id="li_profit" show={ show.profit } class="item">Profit</th>
                <th id="li_offered" show={ show.offered } class="item">Angebot</th>
                <th id="li_requested" show={ show.requested } class="item">Nachfrage</th>
            </tr>
        </thead>
        <tbody>
            <tr each={item in data} class="row" id="{ item.data_id }">
                <td show={ show.id } class="item">{ item.data_id }</td>
                <td show={ show.name } class="item">{ item.name }</td>
                <td show={ show.rarity } class="item">{ item.rarity }</td>
                <td show={ show.restriction_level } class="item">{ item.restriction_level }</td>
                <td show={ show.type } class="item">{ item.type_id }:{ item.sub_type_id }</td>
                <td show={ show.last_price_change } class="item">{ new Date(item.price_last_changed.slice(0, -3).replace(/-/g,'/')).toLocaleString(); }</td>
                
                <td show={ show.offer_price } class="item">{ item.max_offer_unit_price }</td>
                <td show={ show.offer_price_change_last_hour } class="item">{ item.offer_price_change_last_hour + "%" }</td>
                <td show={ show.offer_price_change_last_hour_raw } class="item">{ ((item.offer_price_change_last_hour/100) * item.max_offer_unit_price).toFixed(2) }</td>

                <td show={ show.sale_price } class="item">{ item.min_sale_unit_price }</td>
                <td show={ show.sale_price_change_last_hour } class="item">{ item.sale_price_change_last_hour + "%" }</td>
                <td show={ show.sale_price_change_last_hour_raw } class="item">{ ((item.sale_price_change_last_hour/100) * item.min_sale_unit_price).toFixed(2) }</td>

                <td show={ show.profit} class="item">{ item.profit.toFixed(2); }</td>
                <td show={ show.offered } class="item">{ item.sale_availability }</td>
                <td show={ show.requested } class="item">{ item.offer_availability }</td>
            </tr>
        </tbody>
    </table>
    <script>
    this.show = {};
    this.show.id = true;
    this.show.name = true;
    this.show.rarity = false;
    this.show.restriction_level = false;
    this.show.type = false;
    this.show.last_price_change = true;
    this.show.offer_price = true;
    this.show.offer_price_change_last_hour = true;
    this.show.offer_price_change_last_hour_raw = true;
    this.show.sale_price_change_last_hour = true;
    this.show.sale_price_change_last_hour_raw = true;
    this.show.sale_price = true;
    this.show.profit = true;
    this.show.offered = true;
    this.show.requested = true;

    this.data = [];
    this.add = (item) => {
        this.data.push(item);
        /*this.update();*/
    };
    this.on("updated", (data) => {});
    </script>
</fr-item-list>
