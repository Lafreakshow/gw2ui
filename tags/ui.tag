<fr-progress-meter>
    <img src="res/loading.gif">
    <span>{ text }</span>
    <script>
    this.text = "loading...";
    this.update();
    this.setText = (text) => {
        if (!(this.text === text)) {
            this.text = text;
            this.update();
        }
    }
    </script>
</fr-progress-meter>
