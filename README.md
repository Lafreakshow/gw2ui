### GW2Spidy UI ###

This is a simple UI for data pulled directly from [gw2spidy.com](https://www.gw2spidy.com/).
The UI features:

*  A sortable table with Name, prices and other Information
*  Calculated possible profits
*  Easy changing of border conditions

***

### Border conditions ###
There are three possible values to limit the shown entries

1.  The minimum supply
2.  The minimum demand
3.  The minimum possible profit

When deciding which items are shown in the table, the algorithm compares current supply, demand and the possible profit of each item with the given minimum values. Items which which do not have at least 200 in Supply and Demand are never in the table. Only items which meet all conditions are shown to the user. Care should be taken when using small values as performance will get worse really fast the slower the values get. My advice: stay above the default of 10000 for supply and demand and 10 for profit. 

Always think about this: the easier it is for an Item to meet the conditions the larger the number of items will grow, which slows the app down.

***

### Download ###

This app is powered by Jquery and Riot.js and (due to the inclusion of jquery and riot.js in the repository) completely self contained. You can find the current latest release in the [download section](https://bitbucket.org/Lafreakshow/gw2ui/downloads). Look for the .zip with the highest version number. Simply extract the archive and open the single html file inside with your favorite Browser. Tests have been done in Chrome and Firefox. most other browsers should work fine too.