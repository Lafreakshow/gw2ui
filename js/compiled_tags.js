riot.tag2('fr-settings', '<div class="container"> <label for="sale_limes">Angebot <input id="sale_limes" name="sale_limes" min="0" value="1000" type="number"> </label> <label for="offer_limes">Nachfrage <input id="offer_limes" name="offer_limes" min="0" value="1000" type="number"> </label> <label for="diff_limes">Profit <input id="diff_limes" name="diff_limes" min="0" value="2" type="number"> </label> </div> <span onclick="{rl}" id="reload_data_button">Daten neu Laden</span>', '', '', function(opts) {
    this.setOfferLimesInput = (val) => {
        this.offer_limes.value = val;
    };
    this.setSaleLimesInput = (val) => {
        this.sale_limes.value = val;
    };
    this.setDiffLimesInput = (val) => {
        this.diff_limes.value = val;
    };

    this.getOfferLimes = () => {
        return this.offer_limes.value;
    };
    this.getSaleLimes = () => {
        return this.sale_limes.value;
    };
    this.getDiffLimes = () => {
        return this.diff_limes.value;
    };
    this.rl = () => {
        this.trigger("reload_data");
    };
});

riot.tag2('fr-progress-meter', '<img src="res/loading.gif"> <span>{text}</span>', '', '', function(opts) {
    this.text = "loading...";
    this.update();
    this.setText = (text) => {
        if (!(this.text === text)) {
            this.text = text;
            this.update();
        }
    }
});

riot.tag2('fr-item-list', '<table class="tablesorter" name="itemtable" id="item_table"> <thead> <tr class="head"> <th id="li_id" show="{show.id}" class="item">ID</th> <th id="li_name" show="{show.name}" class="item">Name</th> <th id="li_rarity" show="{show.rarity}" class="item">rarity</th> <th id="li_restriction" show="{show.restriction_level}" class="item">restriction level</th> <th id="li_type" show="{show.type}" class="item">type:sub-type</th> <th id="li_last_price_change" show="{show.last_price_change}" class="item">Letzte Preisänderung</th> <th id="li_offer_price" show="{show.offer_price}" class="item">Angebotspreis</th> <th id="li_sale_price" show="{show.sale_price}" class="item">Nachfragepreis</th> <th id="li_profit" show="{show.profit}" class="item">Profit</th> <th id="li_offered" show="{show.offered}" class="item">Angebot</th> <th id="li_requested" show="{show.requested}" class="item">Nachfrage</th> </tr> </thead> <tbody> <tr each="{item in data}" class="row" id="{item.data_id}"> <td show="{show.id}" class="item">{item.data_id}</td> <td show="{show.name}" class="item">{item.name}</td> <td show="{show.rarity}" class="item">{item.rarity}</td> <td show="{show.restriction_level}" class="item">{item.restriction_level}</td> <td show="{show.type}" class="item">{item.type_id}:{item.sub_type_id}</td> <td show="{show.last_price_change}" class="item">{new Date(item.price_last_changed.slice(0, -3).replace(/-/g,\'/\')).toLocaleString();}</td> <td show="{show.offer_price}" class="item">{item.max_offer_unit_price}</td> <td show="{show.sale_price}" class="item">{item.min_sale_unit_price}</td> <td show="{show.profit}" class="item">{item.profit.toFixed(2);}</td> <td show="{show.offered}" class="item">{item.sale_availability}</td> <td show="{show.requested}" class="item">{item.offer_availability}</td> </tr> </tbody> </table>', '', '', function(opts) {
    this.show = {};
    this.show.id = true;
    this.show.name = true;
    this.show.rarity = false;
    this.show.restriction_level = false;
    this.show.type = false;
    this.show.last_price_change = true;
    this.show.offer_price = true;
    this.show.sale_price = true;
    this.show.profit = true;
    this.show.offered = true;
    this.show.requested = true;

    this.data = [];
    this.add = (item) => {
        this.data.push(item);

    };
    this.on("updated", (data) => {});
});
