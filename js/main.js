'use strict';
console.log(riot.version);

var logic = {};

var ch = null;
var listPanel, progressMeter, settingsPanel;

logic.init = () => {
    listPanel = riot.mount("fr-item-list")[0];
    riot.mount("fr-toggle-dev-tools");
    progressMeter = riot.mount("fr-progress-meter")[0];
    settingsPanel = riot.mount("fr-settings")[0];

    $("fr-item-list").hide();
    $("#settings_wrapper").hide();
    settingsPanel.setOfferLimesInput(10000);
    settingsPanel.setSaleLimesInput(10000);
    settingsPanel.setDiffLimesInput(15);

    logic.settingsPanel = settingsPanel;
    logic.progressMeter = progressMeter;
    logic.listPanel = listPanel;
};

/* settings: 
 -> offer_limes - Demand (best > 1000)
 -> sale_limes - Supply (best > 1000)
 -> diff_limes - least profit margin (best > 5) 
 avoid small values. it WILL leas to REALLY bad performance */

logic.loadAllItems = (settings) => {
    $("fr-item-list").hide();
    listPanel.unmount(true);
    listPanel = riot.mount("fr-item-list")[0];
    var JSON_URL_ITEMS = "http://www.gw2spidy.com/api/v0.9/json/all-items/all?sort_trending=sale";
    $("#settings_wrapper").fadeOut();
    $("fr-progress-meter").fadeIn();
    progressMeter.setText("Downloading");
    $.getJSON(JSON_URL_ITEMS).done((data, testStatus, jqXHR) => {
        progressMeter.setText("Processing");
        var step = 100 / data.count;
        var sanitizedData, item_count = 0;
        var list = [];

        var offer_limes = settings.offer_limes;
        var sale_limes = settings.sale_limes;
        var diff_limes = settings.diff_limes;

        console.log(offer_limes, sale_limes, diff_limes);

        for (var i = data.results.length - 1; i >= 0; i--) {
            /*list.add(data.results[i])*/
            if (data.results[i].offer_availability >= offer_limes && data.results[i].sale_availability >= sale_limes) {
                /* sale = demand, offer = supply */
                var supply_price = data.results[i].max_offer_unit_price;
                var demand_price = data.results[i].min_sale_unit_price;
                var diff = demand_price - (supply_price + demand_price * 0.15);
                console.log(diff);
                /*var diff = data.results[i].min_sale_unit_price - data.results[i].max_offer_unit_price / .85;*/
                if (diff >= diff_limes) {
                    data.results[i].profit = diff;
                    list.push(data.results[i]);
                    item_count += 1;
                }
            }
        };
        progressMeter.setText("Rendering");
        for (var i = list.length - 1; i >= 0; i--) {
            listPanel.add(list[i]);
        };
        listPanel.update();
        $("table").tablesorter({
            sortList: [
                [0, 0]
            ]
        });
        $("fr-progress-meter").fadeOut();
        $("fr-item-list").fadeIn();
        $("#settings_wrapper").fadeIn();
    }).fail((jqXHR, textStatus, error) => {
        console.log(error);
        progressMeter.setText("Daten können nicht Geladen werden");
        $("#settings_wrapper").fadeIn();
    });
};
